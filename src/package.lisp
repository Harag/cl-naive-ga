(in-package :common-lisp-user)

(defpackage :cl-naive-ga
  (:use :cl)
  (:export
   :generate-qr-file
   :generate-qr-html
   :generate-time-code
   :test-time-code
   :generate-backup-code
   :generate-backup-codes
   :test-backup-code
   :test-backup-codes))
