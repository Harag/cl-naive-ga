(in-package :cl-naive-ga)

(defun generate-qr-file (app-url user-secret issuer path
                         &key (version 1) (level :level-m)
                         (mode nil) (pixsize 9) (margin 8))
  "This function creates a png file representing the QR code that can
be used to register the app with Google Authenticator. Its up to the
user to display the file for consumption.

The app-url is a url that represents your application, something like
https:/my-app.com or https://koos@my-app.com.

The function will return the path if successfull else it will return
nil.

The user-secret is a base32 string representing a secret code you need to generate for your user.

Path is the full path to the file you want created.

version may be adapted to accommodate the encoding data.

level is the error correction level, which should be one of :level-l,
:level-m, :level-q, or :level-h.

For most of the time, MODE should be left nil.

pix size is number of pixels for each QR-SYMBOL module,

margin is number of pixels for each side of the QRcode quiet zone.

"
  (cl-qrencode:encode-png
   (format nil "~A?secret=~A&issuer=~A"
           app-url
           user-secret
           issuer)
   :fpath path :version version :level level
   :mode mode :pixsize pixsize :margin margin)

  (when (probe-file path)
    path))

(defun generate-qr-html (app-url user-secret issuer path image-path
                         &key (version 1) (level :level-m)
                         (mode nil) (pixsize 9) (margin 8))
  "Convenience function that generates html that can be parsed into a
web application to display the QR code on a web page.

The function will return html if successfull else it will return
nil.

The app-url is a url that represents your application, something like
https:/my-app.com or https://koos@my-app.com.

The user-secret is a string representing a secret code you need to generate for your user.

Path is the full path to the file you want created.

image-path is what will go into the scr of the image.

version may be adapted to accommodate the encoding data.

level is the error correction level, which should be one of :level-l,
:level-m, :level-q, or :level-h.

For most of the time, MODE should be left nil.

pix size is number of pixels for each QR-SYMBOL module,

margin is number of pixels for each side of the QRcode quiet zone.
"
  (when (generate-qr-file app-url user-secret issuer path
                          :version version :level level
                          :mode mode :pixsize pixsize :margin margin)

    (cl-who:with-html-output-to-string (*standard-output* nil :indent nil)
      (:img :src (format nil "~A" image-path)))))

(defun generate-time-code (user-secret)
  "A function that is used to generate a code using the user's secret
code. The user-secret is converted to bytes to use with totp:totp."
  (totp:totp (format nil "~{~2,'0x~}"
                     (coerce (cl-base32:base32-to-bytes user-secret) 'list))))

(defun test-time-code (user-secret code)
  "A funciton that compares a given code to a code generated for the user-secret.

Obviously the code comparison is time sensitive.

The code is whatever code Google Authenticator app (or some other app) gave the user."
  (let ((gen-code (prin1-to-string (generate-time-code user-secret))))
    (if (equal gen-code code)
        (values t code)
        (values nil gen-code))))

(defun generate-backup-code (user-secret counter)
  "A function that is used to generate a one time backup code using the user's secret
code. The user-secret is converted to bytes to use with hotp:hotp.

The if a counter is reused the code will be the same. Its up to the
user of this package to keep on increasing the counter for different
codes."
  (hotp:hotp (format nil "~{~2,'0x~}"
                     (coerce (cl-base32:base32-to-bytes user-secret) 'list))
             counter))

(defun generate-backup-codes (user-secret counter-start number-of-codes)
  "This function sequentially generates one time backup codes from
the counter-start to the number of codes.

Once again its up to the user to save codes and their counts for later
testing and to preferably invalidate used codes."
  (let ((i (1- counter-start))
        (codes))

    (loop repeat number-of-codes
          do (push (list :code (generate-backup-code user-secret (incf i))
                         :counter i)
                   codes))

    codes))

(defun test-backup-code (user-secret code counter)
  "A funciton that compares a given code to a code generated for the user-secret.

Obviously the code comparison is dependent on the counter.

The code is whatever code Google Authenticator app (or some other app)
gave the user."
  (let ((gen-code (generate-backup-code user-secret counter)))

    (if (equalp (if (stringp gen-code)
                    gen-code
                    (format nil "~A" gen-code))
                (if (stringp code)
                    code
                    (format nil "~A" code)))
        (values t code)
        (values nil gen-code))))

(defun test-backup-codes (user-secret code counter-start number-of-codes)
  "This function sequentially tests backup codes from the
counter-start to the number of codes."
  (let ((i (1- counter-start)))

    (loop repeat number-of-codes
          do (when (equal (generate-backup-code user-secret (incf i)) code)
               (return-from test-backup-codes t)))

    nil))
