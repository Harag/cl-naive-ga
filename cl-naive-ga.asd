(defsystem "cl-naive-ga"
  :description "Package to use 2 step authentication with google authenticator app. This package uses cl-qrencode and cl-one-time-passwords to generate qr codes and validate codes."
  :version "2022.1.12"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-qrencode :cl-one-time-passwords :cl-base32 :cl-who)
  :components (
               (:file "src/package")
               (:file "src/ga" :depends-on ("src/package"))))

