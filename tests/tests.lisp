(in-package :cl-naive-ga.tests)

(defparameter *url* "https://koos@my-app.com")
(defparameter *secret* "JBSWY3DPEHPK3PXP")
(defparameter *issuer* "naive-ga")

(defun synchronize-time ()
  (loop :named wait-different
        :with tc := (generate-time-code *secret*)
        :while (= tc (generate-time-code *secret*))
        :do (sleep 1))
  (loop :named wait-equal
        :with tc := (generate-time-code *secret*)
        :until (= tc (generate-time-code *secret*))
        :do (sleep 1)))

(testsuite  :ga
            (testcase :qr-code-file
                      :expected "~/qr.png"
                      :actual (generate-qr-file *url* *secret* *issuer* "~/qr.png")
                      :info "Test to see if qr file is created.")
            (testcase :qr-code-html
                      :expected "<img src='/images/qr.png' />"
                      :actual (generate-qr-html *url* *secret* *issuer* "~/qr.png" "/images/qr.png")
                      :info "Tests html created for qr file.")
            (testcase :time-code-gen
                      :expected t
                      :actual (progn
                                (synchronize-time)
                                (equalp (generate-time-code *secret*) (generate-time-code *secret*)))
                      :info "Test to see if time code is created the same.")
            (testcase :time-code-test
                      :expected t
                      :actual (progn
                                (synchronize-time)
                                (test-time-code *secret* (prin1-to-string (generate-time-code *secret*))))
                      :info "Test backup code test.")
            (testcase :backup-code-gen
                      :expected t
                      :actual (equalp (generate-backup-code *secret* 1) (generate-backup-code *secret* 1))
                      :info "Test to see if time code is created the same.")
            (testcase :backup-code-test
                      :expected t
                      :actual (test-backup-code *secret* (generate-backup-code *secret* 1) 1)
                      :info "Test backup code test.")
            (testcase :backup-codes
                      :expected '((:CODE 602287 :COUNTER 2) (:CODE 996554 :COUNTER 1))
                      :actual (generate-backup-codes *secret* 1 2)
                      :info "Test backup code test.")
            (testcase :backup-code-tests
                      :expected t
                      :actual (test-backup-codes *secret* (generate-backup-code *secret* 15) 10 10)
                      :info "Test backup codes test."))

;;(report (run))
