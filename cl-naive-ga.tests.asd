(defsystem "cl-naive-ga.tests"
  :description "Tests for cl-naive-ga."
  :version "2022.1.12"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on (:cl-naive-ga :cl-naive-tests)
  :components (
               (:file "tests/package")
               (:file "tests/tests" :depends-on ("tests/package"))))

