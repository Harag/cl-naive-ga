(defparameter *url* "https://koos@my-app.com")
(defparameter *secret* "JBSWY3DPEHPK3PXP")

;;Generate a qr file
(generate-qr-file *url* *secret* "~/qr.png")

;;Generates a qr file and returns image html looking like this
;;"<img src='/images/qr.png' />"
(generate-qr-html *url* *secret* "~/qr.png" "/images/qr.png")

;;Generate a time sensitive code and test if it is ok.
(test-time-code *secret* (generate-time-code *secret*))

;;Generate a backup code, which is not time sensitive but counter
;;sensitive, and test if its ok
(test-backup-code *secret* (generate-backup-code *secret* 1) 1)

;;Generate a block of backup codes
(generate-backup-codes *secret* 1 2)

;;Test backup code against a range of possible backup codes.
(test-backup-codes *secret* (generate-backup-code *secret* 15) 10 10)
